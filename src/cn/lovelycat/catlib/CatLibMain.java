package cn.lovelycat.catlib;

import cn.lovelycat.catlib.catLib.MsgUtil;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.plugin.java.JavaPlugin;

public class CatLibMain extends JavaPlugin {
    public static CatLibMain instance;

    private static ConsoleCommandSender ccs;

    public static CatLibMain getInstance() {
        return instance;
    }

    @Override
    public void onLoad() {
        instance = this;
        ccs = getServer().getConsoleSender();
    }

    @Override
    public void onEnable() {
        MsgUtil.psendMsg(ccs,"&aCatLib前置已加载！");
    }
}
