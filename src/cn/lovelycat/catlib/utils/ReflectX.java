package cn.lovelycat.catlib.utils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectX {
    // 尝试着用反射...

    public static Class<?> getClass(String name) {
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Method getMethod(Class<?> classObject, String methodName) {
        try {
            return classObject.getMethod(methodName);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Field getDeclaredFiled(Class<?> classObject, String paramName) {
        try {
            return classObject.getDeclaredField(paramName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Field getDeclaredFiled(String className, String paramName) {
        try {
            return Class.forName(className).getDeclaredField(paramName);
        } catch (NoSuchFieldException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Field getDeclaredFiledClass(Class<?> classObject, String paramName) {
        try {
            return classObject.getDeclaredField(paramName);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getDeclaredVariable(Field field, Object object) {
        try {
            return field.get(object);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getDeclaredVariable(Class<?> classObject, String paramName, Object object) {
        try {
            return getDeclaredFiledClass(classObject,paramName).get(object);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
