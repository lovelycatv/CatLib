package cn.lovelycat.catlib.utils;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class Base64X {
    final static BASE64Encoder encoder = new BASE64Encoder();
    final static BASE64Decoder decoder = new BASE64Decoder();

    public static String encode(String code){
        String res = null;
        try {
            res = encoder.encode(code.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return res;
    }
    public static String decode(String code){
        String res = null;
        try {
            res = new String(decoder.decodeBuffer(code),"UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }
}
