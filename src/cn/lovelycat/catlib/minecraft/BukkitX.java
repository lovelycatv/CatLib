package cn.lovelycat.catlib.minecraft;

import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BukkitX {
    private static String core_version = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];

    public static String getBukkitVersion() {
        return Bukkit.getBukkitVersion().split("-")[0];
    }

    public static String getCoreVersion() {
        return core_version;
    }

    public static List<Player> getOnlinePlayers() {
        Collection<? extends Player> collection = Bukkit.getOnlinePlayers();
        List<Player> l = new ArrayList<>(collection);
        return l;
    }

    public static void registerListener(JavaPlugin plugin, Listener listener) {
        Bukkit.getPluginManager().registerEvents(listener,plugin);
    }

    public static void registerListeners(JavaPlugin plugin, List<Listener> listenersList) {
        for (Listener listener : listenersList) {
            Bukkit.getPluginManager().registerEvents(listener, plugin);
        }
    }

    public static void setCommandExecutor(JavaPlugin plugin, String cmdName, CommandExecutor commandExecutor) {
        plugin.getCommand(cmdName).setExecutor(commandExecutor);
    }

    public static void setCommandExecutors(JavaPlugin plugin, List<String> cmdNameList, List<CommandExecutor> commandExecutors) {
        int i=0;
        for (CommandExecutor commandExecutor : commandExecutors) {
            plugin.getCommand(cmdNameList.get(i)).setExecutor(commandExecutor);
            i++;
        }
    }

    public static ItemMeta getItemMeta(Material material) {
        return Bukkit.getItemFactory().getItemMeta(material);
    }

    public static void setPluginStatus(JavaPlugin plugin,String name,boolean bool) {
        Plugin[] plugins = Bukkit.getPluginManager().getPlugins();
        for (int i=0;i<plugins.length;i++) {
            if (plugins[i].getName().equalsIgnoreCase(name)) {
                int finalI = i;
                Bukkit.getScheduler().runTask(plugin, () -> {
                    if (bool) {
                        plugins[finalI].getPluginLoader().enablePlugin(plugins[finalI]);
                    }else {
                        plugins[finalI].getPluginLoader().disablePlugin(plugins[finalI]);
                    }
                });

            }
        }
    }

    public static BanList getBanList(BanList.Type type) {
        return Bukkit.getBanList(type);
    }

    public static PlayerInventory getPlayerInventory(Plugin plugin, String name) {
        return Bukkit.getPlayer(name) == null ? null : Bukkit.getPlayer(name).getInventory();
    }


}
