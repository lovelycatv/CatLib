package cn.lovelycat.catlib.minecraft;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InventoryX {
    public static boolean takeItem(Inventory inventory, ItemStack itemStack, int value) {
        if (inventory == null || itemStack == null) {
            return false;
        }
        for (int i=0;i<inventory.getSize();i++) {
            ItemStack invItem = inventory.getItem(i);
            if (invItem == null) {
                continue;
            }
            if (itemStack.isSimilar(invItem)) {
                if (value < invItem.getAmount()) {
                    invItem.setAmount(invItem.getAmount() - value);
                }else if (value >= invItem.getAmount()) {
                    inventory.setItem(i,null);
                }
                return true;
            }
        }
        return false;
    }

    public static boolean takeItem(Inventory inventory, int position, int amount) {
        if (inventory == null || inventory.getItem(position) == null) {
            return false;
        }
        ItemStack item = inventory.getItem(position);
        if (amount < item.getAmount()) {
            item.setAmount(item.getAmount() - amount);
        }else if (amount >= item.getAmount()) {
            inventory.setItem(position,null);
        }
        return true;
    }

    public static boolean copyItem(Inventory inventory, int position, int amount) {
        if (inventory == null || inventory.getItem(position) == null) {
            return false;
        }
        ItemStack item = inventory.getItem(position);
        int maxStack = item.getMaxStackSize();
        int nowAmount = item.getAmount();
        if (amount + nowAmount > maxStack) {
            int more = amount + nowAmount - maxStack;
            int packCount = (int) Math.floor(more / maxStack);
            int packLast = more % maxStack;
            int allPackCount = packCount + (packLast == 0 ? 0 : 1);
            for (int i=0;i<allPackCount;i++) {
                ItemStack given = item.clone();
                if (i == allPackCount - 1 && packLast != 0) {
                    given.setAmount(packLast);
                    giveItem(inventory,given);
                }else {
                    given.setAmount(maxStack);
                    giveItem(inventory,given);
                }
            }
            ItemStack given = item.clone();
            given.setAmount(given.getMaxStackSize());
            inventory.setItem(position,given);
        }else {
            item.setAmount(item.getAmount() + amount);
        }
        return true;
    }

    public static boolean giveItem(Inventory inventory, ItemStack itemStack) {
        if (inventory == null || itemStack == null) {
            return false;
        }
        for (int i=0;i<inventory.getSize();i++) {
            ItemStack invItem = inventory.getItem(i);
            if (invItem == null) {
                inventory.setItem(i,itemStack);
                return true;
            }
        }
        return false;
    }

    public static int getItemPosition(Inventory inventory, ItemStack itemStack) {
        if (inventory == null || itemStack == null) {
            return -1;
        }
        for (int i=0;i<inventory.getSize();i++) {
            ItemStack invItem = inventory.getItem(i);
            if (invItem == null) {
                continue;
            }
            if (itemStack.isSimilar(invItem)) {
                return i;
            }
        }
        return -1;
    }

    public static int getItemCountInInventory(Inventory inventory, ItemStack itemStack) {
        if (inventory == null || itemStack == null) {
            return -1;
        }
        int count = 0;
        for (int i=0;i<inventory.getSize();i++) {
            ItemStack invItem = inventory.getItem(i);
            if (invItem == null) {
                continue;
            }
            if (itemStack.isSimilar(invItem)) {
                count += invItem.getAmount();
            }
        }
        return count;
    }
}
