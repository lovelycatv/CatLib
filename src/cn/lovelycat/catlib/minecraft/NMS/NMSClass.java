package cn.lovelycat.catlib.minecraft.NMS;

import cn.lovelycat.catlib.utils.ReflectX;

import java.util.ArrayList;
import java.util.List;

public class NMSClass {
    public static final String package_nms = "net.minecraft.server";
    public static final String package_obc = "org.bukkit.craftbukkit";

    // org.bukkit.craftbukkit
    public static final String CraftPlayer = "entity.CraftPlayer";

    // net.minecraft.server
    public static final String PlayerInventory = "PlayerInventory";
    public static final String IInventory = "IInventory";
    public static final String ItemStack = "ItemStack";
    public static final String EntityPlayer = "EntityPlayer";
    public static final String EntityHuman = "EntityHuman";

    private static List<String> nmsList = new ArrayList<>();
    private static List<String> obcList = new ArrayList<>();

    public static void install() {
        obcList.add(CraftPlayer);

        nmsList.add(PlayerInventory);
        nmsList.add(IInventory);
        nmsList.add(ItemStack);
        nmsList.add(EntityPlayer);
        nmsList.add(EntityHuman);
    }

    public static String makePackage(String objectName,String version) {
        if (nmsList.size() == 0 || obcList.size() == 0) {
            install();
        }
        for (String name : nmsList) {
            if (name.equals(objectName)) {
                return package_nms + "." + version + "." + name;
            }
        }
        for (String name : obcList) {
            if (name.equals(objectName)) {
                return package_obc + "." + version + "." + name;
            }
        }
        return null;
    }

    public static Class<?> makePackageClass(String objectName,String version) {
        return ReflectX.getClass(makePackage(objectName,version));
    }
}
