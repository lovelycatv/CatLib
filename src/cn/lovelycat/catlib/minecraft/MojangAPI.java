package cn.lovelycat.catlib.minecraft;

import cn.lovelycat.catlib.utils.Base64X;
import cn.lovelycat.catlib.utils.HttpClient;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class MojangAPI {
    public String Mojang_getServerStateForJson() {
        return HttpClient.get("https://status.mojang.com/check");
    }
    public String Mojang_getPlayerUUID(String name) {
        return JSONObject.parseObject(HttpClient.get("https://api.mojang.com/users/profiles/minecraft/"+name)).getString("id");
    }
    public String Mojang_getPlayerSkinCapeForJson(String uuid) {
        return HttpClient.get("https://sessionserver.mojang.com/session/minecraft/profile/"+uuid);
    }
    public String Mojang_getPlayerHistroyForJson(String uuid) {
        return HttpClient.get("https://api.mojang.com/user/profiles/"+uuid+"/names");
    }

    public enum SkinType {
        SKIN,
        CAPE
    }

    public String Mojang_getPlayerSkin(String uuid, SkinType skinType){
        String data = "";
        data = HttpClient.get("https://sessionserver.mojang.com/session/minecraft/profile/"+uuid);
        JSONArray properties = JSONArray.parseArray(JSONObject.parseObject(data).getJSONArray("properties").toJSONString());
        if (properties == null || properties.size() == 0) {
            return null;
        }else {
            // BASE 64
            JSONObject textures = JSONObject.parseObject(Base64X.decode(properties.getJSONObject(0).getJSONObject("value").toJSONString())).getJSONObject("textures");
            JSONObject skin = textures.getJSONObject("SKIN");
            JSONObject cape = textures.getJSONObject("CAPE");
            if (skinType.equals(SkinType.SKIN)) {
                data = skin == null ? "" : skin.getString("url");
            }else {
                data = cape == null ? "" : cape.getString("url");
            }
        }
        return data;
    }
}
