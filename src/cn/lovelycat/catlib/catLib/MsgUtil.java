package cn.lovelycat.catlib.catLib;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.command.ConsoleCommandSender;

public class MsgUtil {
    public static String p_prefix = "&b[CatLib]&f";

    public static void psendMsg(ConsoleCommandSender consoleCommandSender, String text) {
        consoleCommandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',p_prefix + text));
    }

    public static void psendMsg(ConsoleCommandSender consoleCommandSender, String text, String prefix) {
        consoleCommandSender.sendMessage(ChatColor.translateAlternateColorCodes('&',prefix + text));
    }
}
