package cn.lovelycat.catlib.minecraft.NMS;

import cn.lovelycat.catlib.utils.ReflectX;
import org.bukkit.entity.Player;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BukkitNMS {
    /*
        我对NMS一无所知 所以NMS这里就记录一下我用到过的NMS/OBC
     */
    public static Object getCraftPlayer(Player player, String version) {
        return NMSClass.makePackageClass(NMSClass.CraftPlayer,version).cast(player);
    }

    public static Object getEntityPlayer(Player player, String version) {
        try {
            Class<?> craftPlayerClass = NMSClass.makePackageClass(NMSClass.CraftPlayer,version);
            Object craftPlayer = craftPlayerClass.cast(player);
            Method getHandle = craftPlayerClass.getMethod("getHandle");
            return getHandle.invoke(craftPlayer);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getEntityPlayer(Object craftPlayerObject, String version) {
        try {
            Method getHandle = NMSClass.makePackageClass(NMSClass.CraftPlayer,version).getMethod("getHandle");
            return getHandle.invoke(craftPlayerObject);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static Object getPlayerInventory(Object entityPlayer, String version) {
        return ReflectX.getDeclaredVariable(NMSClass.makePackageClass(NMSClass.EntityHuman,version),"inventory",entityPlayer);
        //ReflectX.getDeclaredVariable(NMSClass.makePackageClass(NMSClass.EntityPlayer,version),"inventory",entityPlayer);
    }

    public static Object getItemStack(Object playerInvenroty, String version, int position) {
        try {
            Method getItem = NMSClass.makePackageClass(NMSClass.IInventory,version).getMethod("getItem", int.class);
            return getItem.invoke(playerInvenroty,position);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;
    }
}
