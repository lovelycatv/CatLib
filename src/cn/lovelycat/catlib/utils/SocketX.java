package cn.lovelycat.catlib.utils;

import java.io.*;
import java.net.Socket;
import java.net.URLEncoder;

public class SocketX {
    private static String mess = null;

    // Usage：
    // ServerSocket serverSocket = new ServerSocket(23333);
    // while (!serverSocket.isClosed()) {
    //     while (true) {
    //         Socket client;
    //         client = serverSocket.accept();
    //         SocketX.SocketServerThread socketServerThread = new SocketX.SocketServerThread(client);
    //         socketServerThread.setSocketServerHandle(receivedMsg -> {
    //             System.out.println(""+receivedMsg);
    //         }
    //      }
    // }

    // Socket socket = new Socket("127.0.0.1",23333);
    // sendMsg(socket,"123");

    public static class SocketClientThread implements Runnable {
        public Socket socket;
        public InputStream inputStream;
        public SocketClientHandle socketClientHandle;
        public SocketClientThread(Socket socket) {
            this.socket = socket;
            try {
                this.inputStream = socket.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public interface SocketClientHandle {
            void onMsgReceived(String receivedMsg);
        }

        public void setSocketClientHandle(SocketClientHandle socketClientHandle) {
            this.socketClientHandle = socketClientHandle;
        }

        @Override
        public void run() {
            try {
                while (inputStream != null) {
                    byte[] buf = new byte[1024];
                    int len = 0;
                    String msg;
                    while ((len = inputStream.read(buf)) != -1) {
                        msg = new String(buf, 0, len);
                        socketClientHandle.onMsgReceived(msg);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public static class SocketServerThread implements Runnable {
        private Socket socket;
        private String receivedMsg = "";

        private SocketServerHandle socketServerHandle;

        public interface SocketServerHandle {
            void onMsgReceive(String receivedMsg);
        }

        public SocketServerThread(Socket socket) {
            this.socket = socket;
        }

        public void setSocketServerHandle(SocketServerHandle socketServerHandle) {
            this.socketServerHandle = socketServerHandle;
        }

        @Override
        public void run() {
            //BufferedReader is = null;
            InputStream is;
            try {
                while (true) {
                    is = socket.getInputStream();
                    if (is != null) {
                        while (is.available() != 0) {
                            byte[] bytes = new byte[1024];
                            is.read(bytes);
                            String string = new String(bytes);
                            if (string != null && !string.equalsIgnoreCase("")) {
                                string = string.replace("eof","");
                                String[] sp = string.split("\\|");
                                for (int i=0;i<sp.length;i++) {
                                    if (i == sp.length - 1) {
                                        break;
                                    }
                                    socketServerHandle.onMsgReceive(sp[i]);
                                }
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    public static String read(Socket socket) {
        InputStream is = null;
        BufferedReader br = null;
        try {
            is = socket.getInputStream();
            br = new BufferedReader(new InputStreamReader(is));
            mess = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mess;
    }

    public static void sendMsg(Socket socket, String msg) {
        OutputStream os = null;
        msg += "|";
        try {
            os = socket.getOutputStream();
            os.write(msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void sendMsg(Socket socket, String msg, Encryptions.EncryptionsType encryptionsType) {
        try {
            switch (encryptionsType) {
                case URLCODE:
                    sendMsg(socket, URLEncoder.encode(msg,"UTF-8"));
                    break;
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
