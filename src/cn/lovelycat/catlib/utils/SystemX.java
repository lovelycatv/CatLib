package cn.lovelycat.catlib.utils;

import java.util.Properties;

public class SystemX {
    private static Properties properties = System.getProperties();

    public static String getOSName() {
        return properties.getProperty("os.name");
    }

    public static String getOSVersion() {
        return properties.getProperty("os.version");
    }

    public static String getArch() {
        return properties.getProperty("os.arch");
    }

    public static String getJavaVersion() {
        return properties.getProperty("java.version");
    }
}
