package cn.lovelycat.catlib.utils;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MysqlX {
    public static boolean isTableExist(Connection connection, String tableName) {
        Connection conn = connection;
        DatabaseMetaData meta = null;
        ResultSet rsTables = null;
        try {
            meta = conn.getMetaData();
            rsTables = meta.getTables(null, null, tableName, null);
            if (rsTables.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

}
